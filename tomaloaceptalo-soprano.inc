\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key c \major

		R1*2  |
		g' 4. e' 16 g' ~ g' 4. r8  |
		r8 f' g' a' 16 g' 8. f' 8 e' d'  |
%% 5
		g' 8 g' g' a' 16 g' ~ g' 2  |
		r8 f' g' a' 16 g' 8. f' 8 e' d'  |
		g' 8 g' g' a' 16 g' ~ g' 2  |
		r8 f' f' f' 16 e' 8. d' 8 c' d'  |
		e' 2 ( d'  |
%% 10
		c' 8. ) r16 f' 8 f' 16 e' 8. d' 8 c' d'  |
		c' 1  |
		r4 g' 8 a' 16 g' ~ g' 2  |
		r8 g' g' f' 16 e' ~ e' 2  |
		r4 f' 8 f' 16 e' 8. d' 8 c' d'  |
%% 15
		e' 1  |
		r4 g' 8 a' g' 4. g' 8  |
		g' 4 f' e' 2  |
		r8 f' f' f' e' d' 4 d' 8  |
		c' 1  |
%% 20
		R1  |
		g' 4. e' 16 g' ~ g' 4. r8  |
		r8 f' g' a' 16 g' 8. f' 8 e' d'  |
		g' 8 g' g' a' 16 g' ~ g' 2  |
		r8 f' g' a' 16 g' 8. f' 8 e' d'  |
%% 25
		g' 8 g' g' a' 16 g' ~ g' 2  |
		r8 f' f' f' 16 e' 8. d' 8 c' d'  |
		e' 2 ( d'  |
		c' 8. ) r16 f' 8 f' 16 e' 8. d' 8 c' d'  |
		c' 1  |
%% 30
		r4 g' 8 a' 16 g' ~ g' 2  |
		r8 g' g' f' 16 e' ~ e' 2  |
		r4 f' 8 f' 16 e' 8. d' 8 c' d'  |
		e' 1  |
		r4 g' 8 a' g' 4. g' 8  |
%% 35
		g' 4 f' e' 2  |
		r8 f' f' f' e' d' 4 d' 8  |
		c' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		% estrofa 1
		Hoy, Se -- ñor, ve -- ni -- mos "a o" -- fre -- cer -- te
		es -- "te hu" -- mil -- de pan,
		"que es" fru -- to del tra -- ba -- jo
		de "la hu" -- ma -- ni -- dad,
		"y es" sig -- no de fra -- ter -- ni -- dad, __
		"fe, es" -- pe -- ran -- "za y" ca -- ri -- dad.

		% estribillo
		Tó -- ma -- lo, a -- cép -- ta -- lo,
		por a -- mor trans -- fór -- ma -- lo
		en el cuer -- po de Je -- sús,
		ben -- di -- to se -- as, Se -- ñor.

		% estrofa 1
		Hoy, Se -- ñor, ve -- ni -- mos "a o" -- fre -- cer -- "te el"
		vi -- no que nos das,
		pa -- ra que lo re -- ci -- bas
		en tu san -- "to al" -- tar,
		"y es" sig -- no de fra -- ter -- ni -- dad, __
		"fe, es" -- pe -- ran -- "za y" ca -- ri -- dad.

		% estribillo
		Tó -- ma -- lo, a -- cép -- ta -- lo,
		por a -- mor trans -- fór -- ma -- lo
		en la san -- gre de Je -- sús,
		ben -- di -- to se -- as, Se -- ñor.
	}
>>

\context Staff = "mezzosoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key c \major

		R1*2  |
		e' 4. f' 16 d' ~ d' 4. r8  |
		r8 c' c' c' 16 d' 8. c' 8 b c'  |
%% 5
		e' 8 e' e' f' 16 d' ~ d' 2  |
		r8 c' c' c' 16 d' 8. c' 8 b c'  |
		e' 8 e' e' f' 16 d' ~ d' 2  |
		r8 c' c' c' 16 d' 8. c' 8 b c'  |
		c' 4 ( e' d' b  |
%% 10
		a 8. ) r16 c' 8 c' 16 d' 8. c' 8 b c'  |
		g 1  |
		r4 e' 8 f' 16 d' ~ d' 2  |
		r8 d' d' d' 16 e' ~ e' 2  |
		r4 c' 8 c' 16 d' 8. c' 8 b c'  |
%% 15
		c' 1  |
		r4 e' 8 f' d' 4. d' 8  |
		d' 4 d' e' 2  |
		r8 f' f' f' e' d' 4 d' 8  |
		c' 1  |
%% 20
		R1  |
		e' 4. f' 16 d' ~ d' 4. r8  |
		r8 c' c' c' 16 d' 8. c' 8 b c'  |
		e' 8 e' e' f' 16 d' ~ d' 2  |
		r8 c' c' c' 16 d' 8. c' 8 b c'  |
%% 25
		e' 8 e' e' f' 16 d' ~ d' 2  |
		r8 c' c' c' 16 d' 8. c' 8 b c'  |
		c' 4 ( e' d' b  |
		a 8. ) r16 c' 8 c' 16 d' 8. c' 8 b c'  |
		g 1  |
%% 30
		r4 e' 8 f' 16 d' ~ d' 2  |
		r8 d' d' d' 16 e' ~ e' 2  |
		r4 c' 8 c' 16 d' 8. c' 8 b c'  |
		c' 1  |
		r4 e' 8 f' d' 4. d' 8  |
%% 35
		d' 4 d' e' 2  |
		r8 f' f' f' e' d' 4 d' 8  |
		c' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzosoprano" {
		% estrofa 1
		Hoy, Se -- ñor, ve -- ni -- mos "a o" -- fre -- cer -- te
		es -- "te hu" -- mil -- de pan,
		"que es" fru -- to del tra -- ba -- jo
		de "la hu" -- ma -- ni -- dad,
		"y es" sig -- no de fra -- ter -- ni -- dad, __
		"fe, es" -- pe -- ran -- "za y" ca -- ri -- dad.

		% estribillo
		Tó -- ma -- lo, a -- cép -- ta -- lo,
		por a -- mor trans -- fór -- ma -- lo
		en el cuer -- po de Je -- sús,
		ben -- di -- to se -- as, Se -- ñor.

		% estrofa 1
		Hoy, Se -- ñor, ve -- ni -- mos "a o" -- fre -- cer -- "te el"
		vi -- no que nos das,
		pa -- ra que lo re -- ci -- bas
		en tu san -- "to al" -- tar,
		"y es" sig -- no de fra -- ter -- ni -- dad, __
		"fe, es" -- pe -- ran -- "za y" ca -- ri -- dad.

		% estribillo
		Tó -- ma -- lo, a -- cép -- ta -- lo,
		por a -- mor trans -- fór -- ma -- lo
		en la san -- gre de Je -- sús,
		ben -- di -- to se -- as, Se -- ñor.
	}
>>

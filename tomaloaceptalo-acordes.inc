\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t
		% intro
		c2 g2 f2 g2

		% estrofa
		c2 g2 f2 g2
		c2 g2 f2 g2
		c2 g2 f2 g2
		c2 g2 f2 g2
		c2 c2:7

		% estribillo
		f2 g2 e2:m a2:m
		f2 g2 c2 c2:7
		f2 g2 e2:m a2:m
		f2 g2
		c2 g2 f2 g2

		% estrofa
		c2 g2 f2 g2
		c2 g2 f2 g2
		c2 g2 f2 g2
		c2 g2 f2 g2
		c2 c2:7

		% estribillo
		f2 g2 e2:m a2:m
		f2 g2 c2 c2:7
		f2 g2 e2:m a2:m
		f2 g2
		c1
	}

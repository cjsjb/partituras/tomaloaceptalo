\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key c \major

		R1*2  |
		g 4. e 16 g ~ g 4. r8  |
		r8 f g a 16 g 8. f 8 e d  |
%% 5
		c' 8 c' c' d' 16 b ~ b 2  |
		r8 f g a 16 g 8. f 8 e d  |
		c' 8 c' c' d' 16 b ~ b 2  |
		r8 a a a 16 g 8. f 8 e f  |
		g 4 ( c' b g  |
%% 10
		f 8. ) r16 a 8 a 16 g 8. f 8 e f  |
		e 1  |
		r4 a 8 c' 16 b ~ b 2  |
		r8 b b b 16 c' ~ c' 2  |
		r4 a 8 a 16 g 8. f 8 e f  |
%% 15
		g 1  |
		r4 b 8 c' b 4. b 8  |
		b 4 b c' 2  |
		r8 a a a g f 4 f 8  |
		e 1  |
%% 20
		R1  |
		g 4. e 16 g ~ g 4. r8  |
		r8 f g a 16 g 8. f 8 e d  |
		c' 8 c' c' d' 16 b ~ b 2  |
		r8 f g a 16 g 8. f 8 e d  |
%% 25
		c' 8 c' c' d' 16 b ~ b 2  |
		r8 a a a 16 g 8. f 8 e f  |
		g 4 ( c' b g  |
		f 8. ) r16 a 8 a 16 g 8. f 8 e f  |
		e 1  |
%% 30
		r4 a 8 c' 16 b ~ b 2  |
		r8 b b b 16 c' ~ c' 2  |
		r4 a 8 a 16 g 8. f 8 e f  |
		g 1  |
		r4 b 8 c' b 4. b 8  |
%% 35
		b 4 b c' 2  |
		r8 a a a g f 4 f 8  |
		e 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		% estrofa 1
		Hoy, Se -- ñor, ve -- ni -- mos "a o" -- fre -- cer -- te
		es -- "te hu" -- mil -- de pan,
		"que es" fru -- to del tra -- ba -- jo
		de "la hu" -- ma -- ni -- dad,
		"y es" sig -- no de fra -- ter -- ni -- dad, __
		"fe, es" -- pe -- ran -- "za y" ca -- ri -- dad.

		% estribillo
		Tó -- ma -- lo, a -- cép -- ta -- lo,
		por a -- mor trans -- fór -- ma -- lo
		en el cuer -- po de Je -- sús,
		ben -- di -- to se -- as, Se -- ñor.

		% estrofa 1
		Hoy, Se -- ñor, ve -- ni -- mos "a o" -- fre -- cer -- "te el"
		vi -- no que nos das,
		pa -- ra que lo re -- ci -- bas
		en tu san -- "to al" -- tar,
		"y es" sig -- no de fra -- ter -- ni -- dad, __
		"fe, es" -- pe -- ran -- "za y" ca -- ri -- dad.

		% estribillo
		Tó -- ma -- lo, a -- cép -- ta -- lo,
		por a -- mor trans -- fór -- ma -- lo
		en la san -- gre de Je -- sús,
		ben -- di -- to se -- as, Se -- ñor.
	}
>>
